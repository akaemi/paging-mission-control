package satellite

import file.Entry
import spock.lang.Specification

class SatelliteManagerSpec extends Specification
{
    def "Test Satellite Manager with one alerting satellite"()
    {
        given:
        List<Entry> entries = [createEntry(1, Component.TSTAT),
                            createEntry(1, Component.TSTAT),
                            createEntry(1, Component.TSTAT)]

        when:
        SatelliteManager satelliteManager = new SatelliteManager(entries)
        List<Alert> alerts = satelliteManager.getAlerts()

        then:
        alerts.size() == 1
    }

    def "Test Satellite Manager with no alerting"()
    {
        given:
        List<Entry> entries = [createEntry(1, Component.TSTAT),
                               createEntry(2, Component.TSTAT),
                               createEntry(1, Component.TSTAT)]

        when:
        SatelliteManager satelliteManager = new SatelliteManager(entries)
        List<Alert> alerts = satelliteManager.getAlerts()

        then:
        alerts.size() == 0
    }

    def createEntry(int satelliteId, Component component)
    {
        return new Entry(new Date(), satelliteId, 100, 50, 100, 50, 200.1, component)
    }
}
