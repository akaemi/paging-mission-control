package satellite

import file.Entry
import spock.lang.Specification

class SatelliteSpec extends Specification
{
    def "Test satellite with no alerts"()
    {
        given:
        int satelliteId = 1

        when:
        Satellite satellite = new Satellite(satelliteId)

        then:
        satellite.getAlerts().size() == 0
    }

    def "Test satellite with alerts: just one alert when there are 6 events in a 5 minute window"()
    {
        given:
        int satelliteId = 1
        Date now = new Date()

        when:
        Satellite satellite = new Satellite(satelliteId)
        satellite.addInfo(Component.TSTAT, createInfo(now))
        satellite.addInfo(Component.TSTAT, createInfo(now))
        satellite.addInfo(Component.TSTAT, createInfo(now))
        satellite.addInfo(Component.TSTAT, createInfo(now))
        satellite.addInfo(Component.TSTAT, createInfo(now))
        satellite.addInfo(Component.TSTAT, createInfo(now))

        then:
        satellite.getAlerts().size() == 1
    }

    def "Test satellite with alerts: two alerts when there are 3 events in two separate 5 minute window"()
    {
        given:
        int satelliteId = 1
        Date now = new Date()
        Date before = now - 5

        when:
        Satellite satellite = new Satellite(satelliteId)
        satellite.addInfo(Component.TSTAT, createInfo(now))
        satellite.addInfo(Component.TSTAT, createInfo(now))
        satellite.addInfo(Component.TSTAT, createInfo(now))
        satellite.addInfo(Component.TSTAT, createInfo(before))
        satellite.addInfo(Component.TSTAT, createInfo(before))
        satellite.addInfo(Component.TSTAT, createInfo(before))

        then:
        satellite.getAlerts().size() == 2
    }

    def createInfo(Date date)
    {
        return new Info(date, 100, 100, 50, 50, 101.1)
    }
}
