package file

import satellite.Component
import spock.lang.Specification

class FileParserSpec extends Specification
{
    def "Test file parsing"()
    {
        given:
        FileReader input = new FileReader("src/test/resources/small_input.txt")

        when:
        List<Entry> entries = FileParser.readFile(input)

        then:
        entries.size() == 2
        verifyEntry(entries[0], new Entry(new Date(1514869265001), 1001, 101, 98, 25, 20, 99.9, Component.TSTAT))
        verifyEntry(entries[1], new Entry(new Date(1514869269521), 1000, 17, 15, 9, 8, 7.8, Component.BATT ))
    }

    def verifyEntry(Entry entry1, Entry entry2)
    {
        return entry1.getTimestamp() == entry2.getTimestamp() &&
                entry1.getComponent() == entry2.getComponent() &&
                entry1.getRawValue() == entry2.getRawValue() &&
                entry1.getRedHighLimit() == entry2.getRedHighLimit() &&
                entry1.getRedLowLimit() == entry2.getRedLowLimit() &&
                entry1.getYellowHighLimit() == entry2.getYellowHighLimit() &&
                entry1.getYellowLowLimit() == entry2.getYellowLowLimit() &&
                entry1.getSatelliteId() == entry2.getSatelliteId()
    }
}
