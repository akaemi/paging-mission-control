package file

import satellite.Alert
import spock.lang.Specification

class AlertWriterSpec extends Specification
{
    def "Test alert writing"()
    {
        given:
        Date date = new Date(1234567891234)
        StringWriter writer = new StringWriter()
        List<Alert> alerts = [new Alert(1, "High", "Comp", date),
                            new Alert(2, "Low", "Comp", date)]

        when:
        AlertWriter.writeAlerts(writer, alerts)

        then:
        writer.toString() == "[\n" +
                "  {\n" +
                "    \"satteliteId\": 1,\n" +
                "    \"severity\": \"High\",\n" +
                "    \"component\": \"Comp\",\n" +
                "    \"timestamp\": \"2009-02-13T17:31:31.234Z\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"satteliteId\": 2,\n" +
                "    \"severity\": \"Low\",\n" +
                "    \"component\": \"Comp\",\n" +
                "    \"timestamp\": \"2009-02-13T17:31:31.234Z\"\n" +
                "  }\n" +
                "]"
    }
}
