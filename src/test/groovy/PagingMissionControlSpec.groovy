import spock.lang.Specification

class PagingMissionControlSpec extends Specification
{
    def "Test EVERYTHING"()
    {
        given:
        FileReader reader = new FileReader("src/test/resources/input.txt")
        StringWriter writer = new StringWriter()

        when:
        PagingMissionControl.process(reader, writer)

        then:
        writer.toString() == "[\n" +
                "  {\n" +
                "    \"satteliteId\": 1000,\n" +
                "    \"severity\": \"RED LOW\",\n" +
                "    \"component\": \"BATT\",\n" +
                "    \"timestamp\": \"2018-01-01T23:01:09.521Z\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"satteliteId\": 1000,\n" +
                "    \"severity\": \"RED HIGH\",\n" +
                "    \"component\": \"TSTAT\",\n" +
                "    \"timestamp\": \"2018-01-01T23:01:38.001Z\"\n" +
                "  }\n" +
                "]"
    }
}
