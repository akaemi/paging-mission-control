package satellite;

import file.Entry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SatelliteManager
{
    private Map<Integer, Satellite> satelliteMap = new HashMap<>();

    public SatelliteManager(List<Entry> entries)
    {
        for (Entry entry: entries)
        {
            addEntry(entry);
        }
    }

    public List<Alert> getAlerts()
    {
        List<Alert> alerts = new ArrayList<>();
        for (Satellite satellite : satelliteMap.values())
        {
            alerts.addAll(satellite.getAlerts());
        }
        return alerts;
    }

    private void addEntry(Entry entry)
    {
        int id = entry.getSatelliteId();

        // If we already have a satellite item for this id, use it: otherwise, create a new one!
        Satellite satellite = satelliteMap.get(id);
        if (satellite == null)
        {
            satellite = new Satellite(id);
            satelliteMap.put(id, satellite);
        }

        // Add this information to the satellite
        satellite.addInfo(entry.getComponent(),
                new Info(entry.getTimestamp(), entry.getRedHighLimit(), entry.getYellowHighLimit(),
                        entry.getYellowLowLimit(), entry.getRedLowLimit(), entry.getRawValue()));
    }
}
