package satellite;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Satellite
{
    private static final int ALERT_WINDOW = 5 * 60 * 1000;

    private int satelliteId;
    List<Info> batteryInfo = new ArrayList<>();
    List<Info> thermostatInfo = new ArrayList<>();

    public Satellite(int satelliteId)
    {
        this.satelliteId = satelliteId;
    }

    public void addInfo(Component type, Info info)
    {
        if (type.equals(Component.BATT))
        {
            batteryInfo.add(info);
        }
        else if (type.equals(Component.TSTAT))
        {
            thermostatInfo.add(info);
        }
    }

    public List<Alert> getAlerts()
    {
        List<Alert> alerts = new ArrayList<>();
        alerts.addAll(getBatteryAlerts());
        alerts.addAll(getThermostatAlerts());
        return alerts;
    }

    /*
    Battery Alert: when there are three battery voltage readings
    that are under the red low limit within a five minute interval.
    */
    private List<Alert> getBatteryAlerts()
    {
        List<Alert> alerts = new ArrayList<>();

        // Collect battery entries that are under the red low limit
        List<Info> lowBatterInfos = batteryInfo.stream()
                .filter(info -> info.getRawValue()<info.getRedLowLimit())
                .collect(Collectors.toList());

        // Now see if there are 3 within 5 minutes
        List<Date> alertDates = threeWithinTimeLimit(lowBatterInfos);
        for (Date date : alertDates)
        {
            alerts.add(new Alert(this.satelliteId, "RED LOW", Component.BATT.name(), date));
        }

        return alerts;
    }

    /*
    Thermostat Alert: when there are three thermostat readings
        that exceed the red high limit within a five minute interval.
     */
    private List<Alert> getThermostatAlerts()
    {
        List<Alert> alerts = new ArrayList<>();

        // Collect thermostat entries that are over the red high limit
        List<Info> highThermostatInfos = thermostatInfo.stream()
                .filter(info -> info.getRawValue()>info.getRedHighLimit())
                .collect(Collectors.toList());

        // Now see if there are 3 within 5 minutes
        List<Date> alertDates = threeWithinTimeLimit(highThermostatInfos);
        for (Date date : alertDates)
        {
            alerts.add(new Alert(this.satelliteId, "RED HIGH", Component.TSTAT.name(), date));
        }

        return alerts;
    }

    /*
    If 3 (or more) things happen in a 5 minute window, return the date of the first item
     */
    private List<Date> threeWithinTimeLimit(List<Info> infoList)
    {
        List<Date> alertDates = new ArrayList<>();

        // Sort the list by date
        infoList.sort(new DateComparator());

        Date lastMatch = null;
        if (infoList.size() >= 3)
        {
            // Start at the beginning - but end 2 items early because we grab 3 items each time through the loop
            for (int i=0; i<infoList.size()-2; i++)
            {
                // Grab date 1 and date 3 from where we are at in the list
                Date date1 = infoList.get(i).getTimestamp();
                Date date3 = infoList.get(i+2).getTimestamp();

                // If we already found a match within this 5 minute window, ignore this potential match
                if (lastMatch != null && (date1.getTime() - lastMatch.getTime() < ALERT_WINDOW))
                {
                    continue;
                }

                // If date1 and date3 are within the 5 minute window, then we have a hit!
                if (date3.getTime() - date1.getTime() < ALERT_WINDOW)
                {
                    alertDates.add(date1);
                    lastMatch = date1;
                }
            }
        }

        return alertDates;
    }

    class DateComparator implements Comparator<Info>
    {
        @Override
        public int compare(Info info1, Info info2)
        {
            return info1.getTimestamp().compareTo(info2.getTimestamp());
        }
    }
}
