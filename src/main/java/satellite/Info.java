package satellite;

import java.util.Date;

public class Info
{
    private Date timestamp;
    private int redHighLimit;
    private int yellowHighLimit;
    private int yellowLowLimit;
    private int redLowLimit;
    private double rawValue;

    public Info(Date timestamp, int redHighLimit, int yellowHighLimit, int yellowLowLimit, int redLowLimit, double rawValue)
    {
        this.timestamp = timestamp;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
    }

    public Date getTimestamp()
    {
        return timestamp;
    }

    public int getRedHighLimit()
    {
        return redHighLimit;
    }

    // Not needed for current use cases
    public int getYellowHighLimit()
    {
        return yellowHighLimit;
    }

    // Not needed for current use cases
    public int getYellowLowLimit()
    {
        return yellowLowLimit;
    }

    public int getRedLowLimit()
    {
        return redLowLimit;
    }

    public double getRawValue()
    {
        return rawValue;
    }
}
