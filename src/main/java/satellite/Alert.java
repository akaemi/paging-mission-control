package satellite;

import java.util.Date;

public class Alert
{
    private int satteliteId;
    private String severity;
    private String component;
    private Date timestamp;

    public Alert(int satteliteId, String severity, String component, Date timestamp)
    {
        this.satteliteId = satteliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }
}
