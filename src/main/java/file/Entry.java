package file;

import satellite.Component;

import java.util.Date;

public class Entry
{
    private Date timestamp;
    private int satelliteId;
    private int redHighLimit;
    private int yellowHighLimit;
    private int yellowLowLimit;
    private int redLowLimit;
    private double rawValue;
    private Component component;

    public Entry(Date timestamp, int satelliteId, int redHighLimit, int yellowHighLimit, int yellowLowLimit,
                     int redLowLimit, double rawValue, Component component)
    {
        this.timestamp = timestamp;
        this.satelliteId = satelliteId;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
    }

    public Date getTimestamp()
    {
        return timestamp;
    }

    public int getSatelliteId()
    {
        return satelliteId;
    }

    public int getRedHighLimit()
    {
        return redHighLimit;
    }

    public int getYellowHighLimit()
    {
        return yellowHighLimit;
    }

    public int getYellowLowLimit()
    {
        return yellowLowLimit;
    }

    public int getRedLowLimit()
    {
        return redLowLimit;
    }

    public double getRawValue()
    {
        return rawValue;
    }

    public Component getComponent()
    {
        return component;
    }
}
