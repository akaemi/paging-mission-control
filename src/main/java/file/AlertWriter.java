package file;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import satellite.Alert;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class AlertWriter
{
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static void writeAlerts(Writer writer, List<Alert> alerts) throws IOException
    {
        Gson gson = new GsonBuilder().setDateFormat(DATE_FORMAT).setPrettyPrinting().create();

        String json = gson.toJson(alerts);
        writer.write(json);
        writer.close();
    }
}
