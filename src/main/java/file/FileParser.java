package file;

import satellite.Component;

import java.io.BufferedReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FileParser
{
    private static final int TIME_COL = 0;
    private static final int ID_COL = 1;
    private static final int RED_HIGH_COL = 2;
    private static final int YELLOW_HIGH_COL = 3;
    private static final int YELLOW_LOW_COL = 4;
    private static final int RED_LOW_COL = 5;
    private static final int RAW_COL = 6;
    private static final int COMPONENT_COL = 7;

    private static final String DATE_FORMAT = "yyyyMMdd HH:mm:ss.SSS";
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT);

    public static List<Entry> readFile(Reader reader)
    {
        List<Entry> entryList = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(reader);
        bufferedReader.lines().forEach(line->{
            entryList.add(createEntry(line));
        });
        return entryList;
    }

    private static Entry createEntry(String line)
    {
        String[] tokens = line.split("\\|");
        Date timestamp = parseDate(tokens[TIME_COL]);
        int id = Integer.parseInt(tokens[ID_COL]);
        int redHighLimit = Integer.parseInt(tokens[RED_HIGH_COL]);
        int yellowHighLimit = Integer.parseInt(tokens[YELLOW_HIGH_COL]);
        int yellowLowLimit = Integer.parseInt(tokens[YELLOW_LOW_COL]);
        int redLowLimit = Integer.parseInt(tokens[RED_LOW_COL]);
        double rawValue = Double.parseDouble(tokens[RAW_COL]);
        Component component = Component.valueOf(tokens[COMPONENT_COL]);

        return new Entry(timestamp, id, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, component);
    }

    private static Date parseDate(String dateString)
    {
        try
        {
            return dateFormatter.parse(dateString);
        }
        catch (ParseException e)
        {
            return null;
        }
    }
}
