import file.AlertWriter;
import file.FileParser;
import satellite.Alert;
import file.Entry;
import satellite.SatelliteManager;

import java.io.*;
import java.util.List;

public class PagingMissionControl
{
    public static void main(String[] args) throws IOException
    {
        String inputFile = "src/main/resources/input.txt";

        process(new FileReader(inputFile), new PrintWriter(System.out, true));
    }

    public static void process(Reader reader, Writer writer) throws IOException
    {
        // Read the file
        List<Entry> entries = FileParser.readFile(reader);

        // Convert the file entries into satellite info
        SatelliteManager satellites = new SatelliteManager(entries);

        // Get alerts
        List<Alert> alerts = satellites.getAlerts();

        // Write the alerts
        AlertWriter.writeAlerts(writer, alerts);
    }
}
